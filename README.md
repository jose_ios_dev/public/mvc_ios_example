# MVCExample

MVCExample is a simple application developed in UIKit and Swift that demonstrates the use of the Model-View-Controller (MVC) design pattern.

## Features

- Displays a description of the user model.
- Allows increasing the user's age.
- Allows decreasing the user's age.

## Compatibility

MVCExample is compatible with devices running iOS 15 and later.

## MVC Design Pattern

MVCExample follows the MVC design pattern to separate the business logic (Model) from the presentation (View) and user interaction (Controller).

- **Model:** Represents the data and business logic. In this case, the items in the list.
- **View:** Displays the user interface and handles data presentation.
- **Controller:** Coordinates the interaction between the view and the model, manages the application logic, and updates the view based on changes in the model.

## Video Tutorials

- [Understanding MVC Design Pattern](https://youtu.be/Ksy7nurSGJ8)
- [MVCExample Functionality Demo](https://youtu.be/EqeHnySj3R4)

## Usage Instructions

1. Clone or download the MVC_iOs_Example repository.
2. Open the MVCExample.xcodeproj file in Xcode.
3. Run the application on the iOS simulator or a physical device.
4. The application will display a  description of the user model.
5. Tap "Increase Age" to increase the age in the user's description.
6. Tap "Decrease Age" to decrease the age in the user's description.
7. The user's description will indicate if this user is of legal age, depending on whether they are older than 17 years or younger than 18.

Enjoy exploring the functionality of MyApp and learning about the MVC design pattern in iOS!

## License

MVCExample is released under the [MIT License](LICENSE).

## Authors

- [José Antonio Caballero Martínez](https://gitlab.com/JoseAntonioCaballero)
