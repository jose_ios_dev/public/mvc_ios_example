//
//  ViewController.swift
//  MVCExample
//
//  Created by José Caballero on 15/03/24.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var lblUserID: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAge: UILabel!
    @IBOutlet weak var lblIsAdult: UILabel!
    var user:UserModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initUserModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    /// Create a test Model
    private func initUserModel() {
        self.user = UserModel(id: Date().description, name: "McLOVIN", age: 17)
        self.setUserDetails()
    }
    
    private func setUserDetails() {
        self.lblUserID.text = self.user?.id
        self.lblUserName.text = self.user?.getName()
        self.lblUserAge.text = String(self.user?.getAge() ?? 0)
        self.lblIsAdult.text = self.user?.isAdult ?? false ? "Usuario +18" : "Usuario < 18"
    }

    @IBAction func userTouchDecreaseAge(_ sender: Any) {
        self.user?.decreaseAge()
        self.setUserDetails()
    }
    
    @IBAction func userTouchIncreaseAge(_ sender: Any) {
        self.user?.increaseAge()
        self.setUserDetails()
    }
    
}

