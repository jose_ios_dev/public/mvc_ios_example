//
//  UserModel.swift
//  MVCExample
//
//  Created by José Caballero on 15/03/24.
//

import Foundation

class UserModel: NSObject {
    var id:String?
    private var name:String?
    private var age:Int?
    /// Define if user is 18+
    var isAdult:Bool {
        get{
            return self.age ?? 0 > 17
        }
    }
    
    init(id: String?, name: String?, age: Int?) {
        self.id = id
        self.name = name
        self.age = age
    }
    
    /// Set New User Name
    func newName(name:String) {
        self.name = name
    }
    
    /// Return User Age
    func getAge() -> Int {
        return self.age ?? 0
    }
    
    /// Return user Name
    func getName() -> String {
        return self.name ?? "No name defined"
    }
    
    /// Increment User Age
    func increaseAge() {
        guard let age = self.age else {
            self.age = 1
            return
        }
        self.age = age < 130 ? age + 1 : age
    }
    
    /// Decrement User Age
    func decreaseAge() {
        guard let age = self.age else {
            self.age = 0
            return
        }
        
        self.age = age > 0 ? age - 1 : 0
    }
    
    deinit {
        debugPrint("<<<< \(self) >>>>")
    }
}
